"use strict";

$(document).ready(function(){

	function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

	let valor = getParameterByName('value');

	$("#obra-individual").append(
		`
                    <img class="imagen-prev-indiv" src="${data_obras[valor].foto}">
                    <div class="datos-obra">
                        <h2>${data_obras[valor].nombre}</h2>
                        <p class="fecha">Año de publicación: ${data_obras[valor].año}</p>
                        <p>
                            <p>Tecnica: ${data_obras[valor].tecnica}</p>
                            <p>Otros detalles: ${data_obras[valor].descripcion}</p>
                            <p>Envío: Sin costes de envío a nivel internacional</p>
                            <p>Dimensiones: 95x80cm</p>
                            <p>${(data_obras[valor].disponibilidad)?`Disponible para Comprar`:`No Dispoible`}</p>
                        </p>
                        <a href="publicaciones.html" class="btn">Volver a las obras</a>                        
                    </div>
		`
		);

    
});