$(document).ready(function(){
	$('#checkadvance').click(function(){
		$('#busqueda-avanzada').slideToggle();
		$('.talleres').toggleClass('bajar');
		$('.mostrar-mapa').toggleClass('ocultar');
    });
    
    const coordenadas = [];

    console.log(getTalleresList().length)

	getTalleresList().forEach(element => {
		coordenadas.push(element.coordenadas);
    });

    mostrarMapa(coordenadas);

    function mostrarMapa(coordenadas){
        let map = L.map('mostrar-mapa').setView(coordenadas[1], 10);

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var cluster = L.markerClusterGroup();
        for (let i = 0; i < coordenadas.length; i++) {
            cluster.addLayers([
                L.marker(coordenadas[i]).bindPopup('Taller: ' + getTalleresList()[i].nombre)
              ])
            
        }
        cluster.addTo(map);
        
    }


    $('#busqueda-taller').on('input', function(e){
        const expresion = new RegExp(e.target.value),
        registros = document.querySelectorAll('.taller');
    
        registros.forEach(registro => {
            registro.style.display = 'none';
            console.log(registro.childNodes)

            if(registro.childNodes[3].textContent.toLowerCase().replace(/\s/g, " ").search(expresion) != -1){
                registro.style.display = 'block';
            }
        })
    })

    $('#localidad-taller').on('input', function(e){
        const expresion = new RegExp(e.target.value),
        registros = document.querySelectorAll('.taller');
    
        registros.forEach(registro => {
            registro.style.display = 'none';

            if(registro.childNodes[9].textContent.toLowerCase().replace(/\s/g, " ").search(expresion) != -1){
                registro.style.display = 'block';
            }
        })
    })

    $('#actividad-taller').on('input', function(e){
        const expresion = new RegExp(e.target.value),
        registros = document.querySelectorAll('.taller');
    
        registros.forEach(registro => {
            registro.style.display = 'none';

            if(registro.childNodes[11].textContent.toLowerCase().replace(/\s/g, " ").search(expresion) != -1){
                registro.style.display = 'block';
            }
        })
    })

    
});