$(document).ready(function(){
    $('#checkadvance').click(function(){
        console.log(10)
        $('#busqueda-avanzada').slideToggle();
        $('.obras').toggleClass('bajar');
    })

    const inputBuscador = document.querySelector('#buscar'),
    selectOriginal = document.querySelector('#select-original'),
    selectVenta = document.querySelector('#select-venta'),
    inputTecninca = document.querySelector('#tecnica-pintura'),
    inputEstilo = document.querySelector('#estilo-pintura');

eventListener();

function eventListener(){
    if(inputBuscador){
        inputBuscador.addEventListener('input', buscarObra);
    }
    if(selectOriginal){
        selectOriginal.addEventListener('change', isOriginal);
    }
    if(selectVenta){
        selectVenta.addEventListener('change', disponible);
    }
    if(inputTecninca){
        inputTecninca.addEventListener('input', buscarTecnica)
    }
    if(inputEstilo){
        inputEstilo.addEventListener('input', buscarEstilo)
    }
}

function buscarObra(e){
    const expresion = new RegExp(e.target.value),
        registros = document.querySelectorAll('.obra');
    
    registros.forEach(registro => {
        registro.style.display = 'none';

        if(registro.childNodes[3].textContent.toLowerCase().replace(/\s/g, " ").search(expresion) != -1){
            registro.style.display = 'block';
        }
    })
}

function isOriginal(){
    var selectedOption = this.options[selectOriginal.selectedIndex];
    var textSelected = selectedOption.text;
    const registros = document.querySelectorAll('.obra');

    registros.forEach(registro => {
        registro.style.display = 'none';

        if(registro.childNodes[15].textContent.search(textSelected) != -1){
            registro.style.display = 'block';
        }
    })
}

function disponible(){
    var selectedOption = this.options[selectVenta.selectedIndex];
    var textSelected = selectedOption.value;
    const registros = document.querySelectorAll('.obra');

    registros.forEach(registro => {
        registro.style.display = 'none';

        if(registro.childNodes[17].textContent.search(textSelected) != -1){
            registro.style.display = 'block';
        }
    })
}

function buscarTecnica(e){
    const expresion = new RegExp(e.target.value),
        registros = document.querySelectorAll('.obra');
    
    registros.forEach(registro => {
        registro.style.display = 'none';

        if(registro.childNodes[9].textContent.toLowerCase().replace(/\s/g, " ").search(expresion) != -1){
            registro.style.display = 'block';
        }
    })
}

function buscarEstilo(e){
    const expresion = new RegExp(e.target.value),
        registros = document.querySelectorAll('.obra');
    
    registros.forEach(registro => {
        registro.style.display = 'none';

        if(registro.childNodes[11].textContent.toLowerCase().replace(/\s/g, " ").search(expresion) != -1){
            registro.style.display = 'block';
        }
    })

}

})

