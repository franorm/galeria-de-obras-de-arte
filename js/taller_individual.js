"use strict";

function bootstrap(coordenada) {

  let map = L.map('mapid').setView(coordenada, 15);

  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);

  var ungsMarker=L.marker(coordenada);
  ungsMarker.addTo(map);
}

$(document).ready(function(){

	let valor = getUrlParams();

	$("#taller-individual").append(
		`
                    <img class="imagen-prev-indiv" src="${getTalleresList()[valor].foto}">
                    <div class="datos-obra">
                        <h2>${getTalleresList()[valor].nombre}</h2>
                        <p class="fecha">Dirección: ${getTalleresList()[valor].direccion}</p>
                        <p>
                            <p>Telefono: ${getTalleresList()[valor].telefono}</p>
                            <p>${getTalleresList()[valor].descripcion}</p>
                        </p>
                    <ul id="activity"><span>Actividades ofrecidas:</span><hr></ul>                  
                    </div>
		`
		);

    getTalleresList()[valor].actividades.forEach(val => $("#activity").append(
            `<li>${val}</li>`
        ));

    bootstrap(getTalleresList()[valor].coordenadas);
});

