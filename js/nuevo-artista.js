$(document).ready(function(){
     const descripcion = document.getElementById('descripcion');
    
    var select = document.getElementById('participacion');
    select.addEventListener('change', function(){
        var selectedOption = this.options[select.selectedIndex];
        if (selectedOption.value === 'taller'){
            descripcion.setAttribute('placeholder', 'Por favor ingrese el nombre de las actividades a realizar');
        }else{
            $('#descripcion-particular').slideToggle();
            descripcion.setAttribute('placeholder', 'Por favor ingrese una descripción extra sobre sus intereses');
        }
    })

    $('#guardar-artista').on('submit', function(e){
        e.preventDefault();
        const nombre = document.getElementById('nombre').value,
            telefono = document.getElementById('telefono').value,
            email = document.getElementById('email').value,
            descripcion=document.getElementById('descripcion').value,
            select = document.getElementById('participacion').value;
        if(nombre.trim() === '' || telefono.trim() === '' || email.trim() === '' || select.trim() === '' || descripcion.trim() === '' ){
            mostrarNotificacion('error', 'Por favor complete todos los campos')
        }else{
            mostrarNotificacion('success', 'Se han ingresado sus datos al sistema, en breve será revisado por el administrador')
        }
    })

    

    function mostrarNotificacion(tipo, mensaje){
        if(tipo === 'error'){
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: mensaje
              })
        }else{
            Swal.fire({
                icon: 'success',
                title: 'Éxito...',
                text: mensaje,
                confirmButtonText: 'Volver al inicio'
              }).then(result => {
                  setTimeout(window.location.href="index.html", 5000)
              })
        }
    }

})