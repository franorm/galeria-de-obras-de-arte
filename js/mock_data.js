var data_obras = [
	{
		"nombre": "La joven de la perla",
		"descripcion": "Se trata de un tronie, un género pictórico típico de la holanda del siglo XVII que significa “rostro” o “expresión”, y que consiste en la simple representación de la cara de un personaje anónimo que no tenían intención de ser un retrato identificable y que se producían para demostrar la pericia de un artista.",
		"autor": "Johannes Vermeer",
		"año": 1667,
		"estilo": "Barroco",
		"tecnica": "Oleo sobre tela",
		"foto": "./img/obra_5.jpg",
		"original": true,
		"disponibilidad" : true,
		"datos": {
			"observaciones": null,
			"tipo": "Pintura",
			"pieza": "Única"
		}
	},
		{
		"nombre": "La noche estrellada",
		"descripcion": "Pintado en junio de 1889, representa la vista desde la ventana orientada al este de su habitación de asilo en Saint-Rémy-de-Provence, justo antes del amanecer, con la adición de un pueblo imaginario.",
		"autor": "Vincent Van Gogh",
		"año": 1889,
		"estilo": "Potimpresionismo",
		"tecnica": "Oleo sobre tela",
		"foto": "./img/obra_6.jpg",
		"original": true,
		"disponibilidad" : true,
		"datos": {
			"observaciones": null,
			"tipo": "Pintura",
			"pieza": "Única"
		}
	},
		{
		"nombre": "M-maybe",
		"descripcion": "Empleando colores primarios y contornos negros bien definidos, elabora las imágenes mediante entramados de puntos según el sistema llamado Benday, lo cual acercó al artista a reinterpretar la tradicional creación pictórica del Puntillismo. Lo anterior puede observarse en obras como “M-Maybe”, cuya imagen fue tomada de una historieta. En ella, Lichtenstein aísla una representación de la secuencia de imágenes, trasladándola ampliada al lienzo. Calca los contornos del modelo elegido, los proyecta con un episcopio en el lienzo y los fija con un pincel. Con plantillas realiza la trama en algunos lugares, o en todo el cuadro, utilizando para ello los puntos Benday. Por último, aplica el color y pinta los contornos de negro. A través de su pintura, Lichtenstein buscaba mostrar el desafío del hombre moderno para poder distinguir entre las imágenes y la realidad: “Si empezamos a identificarnos con todas las imágenes que circulan, acabaremos por desconectarnos de la realidad”, decía el pintor.",
		"autor": "Roy Lichtenstein",
		"año": 1965,
		"estilo": "Arte pop",
		"tecnica": "Magma sobre tela",
		"foto": "./img/obra_7.jpg",
		"original": true,
		"disponibilidad" : false,
		"datos": {
			"observaciones": null,
			"tipo": "Pintura",
			"pieza": "Única"
		}
	},
		{
		"nombre": "Gótico estadounidense",
		"descripcion": "El cuadro ilustra a un granjero sujetando una horca y a una mujer rubia, que unos interpretan como su esposa y otros como su hija, enfrente de una casa de estilo gótico rural. Es una de las imágenes más conocidas del arte estadounidense del siglo XX y se ha convertido en un icono en la cultura popular, siendo una de las imágenes del arte moderno estadounidense más reconocidas y parodiadas.",
		"autor": "Grant Wood",
		"año": 1930,
		"estilo": "Neogótico",
		"tecnica": "Óleo sobre aglomerado de tela",
		"foto": "./img/obra_8.jpg",
		"original": false,
		"disponibilidad" : true,
		"datos": {
			"observaciones": null,
			"tipo": "Pintura",
			"pieza": "Única"
		}
	},
		{
		"nombre": "El hijo del hombre",
		"descripcion": "La pintura se compone de un hombre con abrigo, corbata roja y bombín de pie delante de un muro. Más allá se ve el mar y un cielo nublado. El rostro del hombre se oscurece en gran parte por una manzana verde flotando. Sin embargo, los ojos del hombre se pueden ver asomando por el borde de la manzana",
		"autor": "René Magritte",
		"año": 1964,
		"estilo": "Surrealismo",
		"tecnica": "Óleo sobre tela",
		"foto": "./img/obra_9.jpg",
		"original": true,
		"disponibilidad" : false,
		"datos": {
			"observaciones": null,
			"tipo": "Pintura",
			"pieza": "Única"
		}
	}

]

var data_talleres = [
	{
		"nombre": "El encuentro",
		"descripcion": "Un espacio de creación de objetos de arte de vidrio y mosaiquismo. Te guiamos en el aprendizaje, a que tengas confianza y liberes tu imaginación. Te enseñamos el uso correcto de las herramientas, la técnica necesaria para el buen uso de los materiales. También te ayudamos a elegir tus diseños, sin intervenir en tus gustos pero aconsejándote sobre colores o materiales.",
		"actividades": ["Escultura", "Vidriería"],
		"direccion": "W. Paunero 2640, Pilar, Provincia de Buenos Aires",
		"horario": "09:00 a 18:00",
		"telefono": "202-555-0115",
		"foto": "./img/taller_1.jpg",
		"coordenadas": [-34.454853, -58.829305]
	},
	{
		"nombre": "Miriam Diaz",
		"descripcion": "En el taller tu creatividad no tiene límites! !Pintados sobre cualquier material!!Bordamos! !!hacemos cerámica A mano!!",
		"actividades": ["Pintura", "Bordado","Escultura"],
		"direccion": "Gral. las Heras 1061, Muñiz, Provincia de Buenos Aires",
		"horario": "08:00 a 17:00",
		"telefono": "202-555-0115",
		"foto": "./img/taller_2.jpg",
		"coordenadas": [-34.546190, -58.704084]
	},
	{
		"nombre": "Florida",
		"descripcion": "Es un taller de artista, rodeado de un jardín verde e inspirador en el barrio de Florida. Un taller de producción, del que se puede entrar y salir cuando se quiere. Un espacio con otro ritmo, donde el encuentro está dado por el placer del trabajo visual.",
		"actividades": ["Dibujo", "Pintura"],
		"direccion": "Agustín Álvarez 2700, Florida, Provincia de Buenos Aires",
		"horario": "09:00 a 18:00",
		"telefono": "202-555-0115",
		"foto": "./img/taller_3.jpg",
		"coordenadas": [-34.540385, -58.493187]
	}
]
