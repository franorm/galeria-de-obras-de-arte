$(document).ready(function(){
    $('#nuevo-taller').on('submit', function(e){
        e.preventDefault();
        
        const nombre = $('#nombre').val(),
            telefono = $('#telefono').val(),
            apertura = $('#apertura').val(),
            cierre = $('#cierre').val(),
            email = $('#email').val(),
            descripcion = $('#descripcion-taller').val();

        //Variables para normalizar direccion    
        const direccion = $('#direccion-taller').val(),
            altura = $('#altura-taller').val(),
            localidad = $('#localidad').val(),
            cruce = $('#calle-cruce').val();

        const listadoActividades = seleccionActividades();

        if(nombre.trim() === '' || telefono.trim() === '' || apertura.trim() === '' || cierre.trim() === '' || email.trim() === '' || descripcion.trim() === '' || listadoActividades.length === 0){
            mostrarNotificacion('error', 'Todos los campos son obligatorios');
        }else{
            const nuevoTaller = {
                'nombre' : nombre,
                'descripcion' : descripcion,
                'actividades' : listadoActividades,
                'horario': apertura + ' a ' + cierre,
                'telefono' : telefono
            }
    
            const localidadNormalizada = localidad.toLowerCase().replace(/ /g, "%20");
        
            var normalizada;
    
            if(direccion.trim() !== '' && altura.trim() !== '' && localidad.trim() === ''){
                normalizada = `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${direccion}%20${altura}`;
                normalizar(normalizada, nuevoTaller);
                return;
            }
            if(direccion.trim() !== '' && altura.trim() !== '' && localidad.trim() !== ''){
                normalizada = `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${direccion}%20${altura},%20${localidadNormalizada}`;
                normalizar(normalizada, nuevoTaller);
                return;
            }if (direccion.trim() !== '' && cruce.trim() !== '' && localidad.trim() !== ''){
                normalizada = `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${direccion}%20y%20${cruce},%20${localidadNormalizada}`;
                normalizar(normalizada, nuevoTaller);
                return;
            }if (direccion.trim() !== '' && localidad.trim() !== '' && altura === '' && cruce === ''){
                console.log('Por favor ingrese altura o una calle de cruce');
            }else if(direccion.trim() !== '' && cruce.trim() !== '' && localidad.trim() === ''){
                normalizada = `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${direccion}%20y%20${cruce}}`;
                normalizar(normalizada, nuevoTaller);
                return;
            }if(direccion.trim() === '' && cruce.trim() === '' && localidad.trim() === '' && altura.trim() === ''){
                mostrarNotificacion('error', 'Por favor complete los campos para verificar su dirección');
            }   
        }
    })

    //Guarda las actividades seleccionadas por el usuario que va a ofrecer su taller
    function seleccionActividades(){
        var listado = [];

        $("input[type=checkbox]:checked").each(function(){
            listado.push(this.value);
        });

        return listado;
    }

    //Funcion que normaliza la direccion con la API USIG
    function normalizar(url, datos) {
        const xhr = new XMLHttpRequest();

        xhr.open('GET', url, true);

        xhr.onload = function() {
            if(this.status === 200){
                var respuesta = JSON.parse(xhr.responseText);
                console.log(respuesta)
                if(respuesta.direccionesNormalizadas.length===1){
                    const direccionNormalizada = {
                        'direccion' : respuesta.direccionesNormalizadas[0].direccion,
                        'coordenadas': [
                            respuesta.direccionesNormalizadas[0].coordenadas.y,
                            respuesta.direccionesNormalizadas[0].coordenadas.x,
                        ]
                    }
                    Object.assign(datos, direccionNormalizada);
                    addTaller(datos);
                    mostrarNotificacion('success', 'El taller se creó correctamente');
                }else if(respuesta.direccionesNormalizadas.length>1){
                    mostrarOpciones(respuesta.direccionesNormalizadas, datos);
                }else if(respuesta.direccionesNormalizadas.length===0){
                    mostrarNotificacion('error', 'La dirección ingresada es inválida. Por favor ingrese una dirección existente');
                }
            }
        }
        xhr.send(datos);   
    }

    //Funcion que muestra opciones en pantalla al usuario en caso de que ingrese una dirección que contenga varias opciones.
    function mostrarOpciones(opciones, datos){
        var direcciones = [];
        console.log(opciones)

        opciones.forEach(opcion => {
            direcciones.push(opcion.direccion);
        });

        Swal.fire({
            title: `Se encontraron ${direcciones.length} direcciones`,
            input: 'select',
            inputOptions: direcciones
        }).then(function(result){
            if (result.value) {
                Swal.fire({
                  icon: 'success',
                  html: 'You selected: ' + direcciones[result.value]
                });
                const respuesta = opciones[result.value];
                console.log(respuesta)
                const normalizada = `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${respuesta.direccion}`;
                normalizar(normalizada, datos)
                
              }
        })
    }

    //Función para mostrar notificación al usuario con la librería SweetAlert
    function mostrarNotificacion(estado, mensaje){
        if(estado === 'error'){
            Swal.fire({
                icon: estado,
                title: 'Error...',
                text: mensaje,
              })
        }else{
            Swal.fire({
                icon: estado,
                title: 'Exito!!',
                text: mensaje,
            }).then((result) => {
                setTimeout(redireccionar(), 5000);
            }
            );
        }
    }

    function redireccionar(){
        window.location.href="talleres.html";
    }

})