"use strict";

function addTaller(datos){
    var taller = {
        'nombre' : datos.nombre,
        'descripcion' : datos.descripcion,
        'actividades' : datos.actividades,
        'direccion': datos.direccion,
        'horario': datos.horario,
        'telefono' : datos.telefono,
        'foto': "./img/empty-taller.png",
        'coordenadas': datos.coordenadas
    }
    data_talleres.push(taller);
    localStorageTalleresList(data_talleres);
}

function getTalleresList(){
    var storedList = localStorage.getItem('localTalleresList');
    if(storedList == null){
    } else {
        data_talleres = JSON.parse(storedList);
    }
    return data_talleres;
}

function localStorageTalleresList(plist){
    localStorage.setItem('localTalleresList', JSON.stringify((plist)));
}

function getUrlParams() {
    let queryString = window.location.search;
    let urlParams = new URLSearchParams(queryString);
    let product = urlParams.get('value')
    return product;
}

$(document).ready(function(){
	data_obras.forEach( (element, index) => {
		$(".obras").append(
			`<div class="obra">
                    <img class="imagen-prev" src="${element.foto}">
                    <h2>${element.nombre}</h2>
                    <p><span>Año de publicación: </span>${element.año}</p>
                    <p><span>Autor: </span>${element.autor}</p>
                    <p class="tecninca"><span>Técnica: </span>${element.tecnica}</p>
                    <p class="estilo"><span>Estilo: </span> ${element.estilo}</p>
                    <p class="descripcion">${element.descripcion}</p>
                    <p class="formalidad">${(element.original)?`Original`:`Réplica`}</p>
                    <p class="d-none">${element.disponibilidad}</p>
                    <a href="obra.html?value=${index}" class="btn">Ir al artículo</a>
                </div>`
			);
    });

    getTalleresList().forEach( (element, index) => {
		$(".talleres").append(
			`<div class="taller">
                    <img class="imagen-prev" src="${element.foto}">
                    <h2>Nombre del taller: ${element.nombre}</h2>
                    <p class="descripcion">${element.descripcion}</p>
                    <p><span>Horario del taller:</span> ${element.horario} </p>
                    <p><span>Dirección del taller:</span> ${element.direccion}</p>
                    <p><span>Actividades:</span> ${element.actividades}</p>
                    <a href="taller.html?value=${index}" class="btn">Ir al taller</a>
                </div>`
            );
    });

})